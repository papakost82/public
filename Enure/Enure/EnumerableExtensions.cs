﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Enure
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Gets the most frequent element of the source using type's default comparer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T GetMostFrequent<T>(this IEnumerable<T> source)
        {
            var d = new Dictionary<T, int>();
            foreach (T obj in source)
            {
                if (!d.ContainsKey(obj))
                {
                    d.Add(obj, 1);
                }
                else
                {
                    d[obj]++;
                }
            }

            T mostFrequent = default(T);
            int max = -1;
            foreach (KeyValuePair<T, int> p in d)
            {
                if (p.Value > max)
                {
                    max = p.Value;
                    mostFrequent = p.Key;
                }
            }
            return mostFrequent;
        }

        /// <summary>
        /// Performs the specified action on each element of the IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            
            var lst = new List<T>(enumerable);
            lst.ForEach(action);
        }

        /// <summary>
        /// Returns the only element of a sequence and throws an exception if there is not exactly one element in the sequence. The exception message can be defined through this method's paramet 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="messageOnNoItems">Exception message on no items found</param>
        /// <param name="messageOnMoreThanOneItems">Excpetion message on more than one items found</param>
        /// <returns></returns>
        public static T Single<T>(this IEnumerable<T> source, string messageOnNoItems, string messageOnMoreThanOneItems)
        {
            return source.Single(i => true, messageOnNoItems, messageOnMoreThanOneItems);

        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition, and throws an exception if more than one such elements exist. The exception message can be defined through this method's parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="col"></param>
        /// <param name="predicate"></param>
        /// <param name="messageOnNoItems">Exception message on no items found</param>
        /// <param name="messageOnMoreThanOneItems">Excpetion message on more than one items found</param>
        /// <returns></returns>
        public static T Single<T>(this IEnumerable<T> col, Func<T, bool> predicate, string messageOnNoItems, string messageOnMoreThanOneItems)
        {
            var items = col.Where(predicate).ToArray();

            if (!items.Any())
            {
                throw new ApplicationException(messageOnNoItems);
            }
            if (items.Length > 1)
            {
                throw new ApplicationException(messageOnMoreThanOneItems);
            }
            return items.Single();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="col"></param>
        /// <param name="messageOnMoreThanOneItems">Excpetion message on more than one items found</param>
        /// <returns></returns>
        public static T SingleOrDefault<T>(this IEnumerable<T> col, string messageOnMoreThanOneItems)
        {
            return col.SingleOrDefault(i => true, messageOnMoreThanOneItems);

        }

        /// <summary>
        /// Returns the only element of a sequence that satisfies a specified condition, and throws an exception if more than one such elements exist. The exception message can be defined through this method's parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="col"></param>
        /// <param name="predicate"></param>
        /// <param name="messageOnMoreThanOneItems">Excpetion message on more than one items found</param>
        /// <returns></returns>
        public static T SingleOrDefault<T>(this IEnumerable<T> col, Func<T, bool> predicate, string messageOnMoreThanOneItems)
        {
            var items = col.Where(predicate).ToArray();

            if (!items.Any())
            {
                return default(T);
            }
            if (items.Length > 1)
            {
                throw new ApplicationException(messageOnMoreThanOneItems);
            }
            return items.Single();
        }

        private static readonly Random RandomGenerator = new Random();
        
        /// <summary>
        /// Sorts the elements of a sequence in random order
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IEnumerable<T> RandomizeOrder<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(item => RandomGenerator.Next());
        }

        /// <summary>
        /// Gets a random element from the sequence or the default value of T if the sequence has no elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static T RandomOrDefault<T>(this IEnumerable<T> lst)
        {
            var array = lst as T[] ?? lst.ToArray();
            if (!array.Any())
            {
                return default(T);
            }
            int randIndex = RandomGenerator.Next(array.Length);
            return array.ElementAt(randIndex);
        }

        /// <summary>
        /// Gets a random element from the sequence and throws an exception if the sequence has no elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T Random<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(item => RandomGenerator.Next()).First();
        }

        /// <summary>
        /// Returns true if the sequence contains any elements, false otherwise
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool IsEmpty<T>(this IEnumerable<T> source)
        {
            return !source.Any();
        }
    }
}
