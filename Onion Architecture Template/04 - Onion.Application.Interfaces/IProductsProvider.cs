﻿using System.Collections.Generic;
using Onion.Domain.Models;

namespace Onion.Application
{
    public interface IProductsProvider
    {
        IList<Product> GetProducts(Currency currency);
    }
}