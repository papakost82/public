﻿using System.Collections.Generic;
using System.Linq;
using Onion;
using Onion.Core.Repositories.Interfaces;
using Onion.Domain.Models;

namespace _04___Onion.Core.Services
{
    public class ProductsService : IProductsService
    {
        private readonly IProductCurrencyConverter _productCurrencyConverter;
        private readonly IProductsRepository _productsRepository;

        public ProductsService(IProductCurrencyConverter productCurrencyConverter, IProductsRepository productsRepository)
        {
            _productCurrencyConverter = productCurrencyConverter;
            _productsRepository = productsRepository;
        }

        public IList<Product> GetProducts(Currency currency)
        {
            var products = _productsRepository.GetProducts();

            return products.Select(product => _productCurrencyConverter.ConvertCurrency(product, currency)).ToList();
        }
    }
}
