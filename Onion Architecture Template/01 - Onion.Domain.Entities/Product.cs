﻿namespace Onion.Domain.Models
{
    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Currency Currency { get; set; }
    }
}
