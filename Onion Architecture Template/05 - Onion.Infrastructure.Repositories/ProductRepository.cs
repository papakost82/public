﻿using System.Collections.Generic;
using Onion.Core.Repositories.Interfaces;
using Onion.Domain.Models;

namespace _02___Onion.Infrastructure.Repositories
{
    public class ProductRepository : IProductsRepository
    {
        public IList<Product> GetProducts()
        {
            return new List<Product>
            {
                new Product {Name = "Necklage 15mm",Price = (decimal)15.5, Currency = Currency.EUR },
                new Product {Name = "Ring 15mm",Price = (decimal)64.51, Currency = Currency.EUR },
            };
        }
    }
}
