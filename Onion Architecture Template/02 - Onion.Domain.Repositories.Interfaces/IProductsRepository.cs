﻿using System.Collections.Generic;
using Onion.Domain.Models;

namespace Onion.Core.Repositories.Interfaces
{
    public interface IProductsRepository
    {
        IList<Product> GetProducts();
    }
}