﻿using System.Collections.Generic;
using Onion.Domain.Models;

namespace Onion
{
    public interface IProductsService
    {
        IList<Product> GetProducts(Currency currency);
    }
}