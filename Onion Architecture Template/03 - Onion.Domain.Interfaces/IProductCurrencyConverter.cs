﻿using Onion.Domain.Models;

namespace Onion
{
    public interface IProductCurrencyConverter
    {
        Product ConvertCurrency(Product product, Currency newCurrency);
    }
}