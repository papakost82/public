﻿using System.Collections.Generic;
using Onion.Domain.Models;

namespace Onion.Application.Services
{
    public class ProductsProvider :IProductsProvider
    {
        private readonly IProductsService _productsService;

        public ProductsProvider(IProductsService productsService)
        {
            _productsService = productsService;
        }

        public IList<Product> GetProducts(Currency currency)
        {
            return _productsService.GetProducts(currency);
        }
    }
}
