﻿using System;
using System.Configuration;

namespace SimpleAppSettings
{
    public static class AppSettingsManager
    {
        /// <summary>
        /// Gets AppSetting as typed value. Will throw ConfigurationsErrorsException when appSetting with the provided key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <returns></returns>
        public static T GetValue<T>(string appSettingsKey)
        {
            return GetConfigurationValueInternal(appSettingsKey, null, default(T), null, false);

        }

        /// <summary>
        /// Gets AppSetting as typed value. Will throw ConfigurationsErrorsException when appSetting with the provided key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <param name="actionOnError">An action that will be excecuted when one of the above errors happen. This is executed before the throw of the ConfigurationErrorsException and can be used e.g. for logging reasons</param>
        /// <returns></returns>
        public static T GetValue<T>(string appSettingsKey, Action<AppSettingReadResult, Exception> actionOnError)
        {
            return GetConfigurationValueInternal(appSettingsKey, null, default(T), actionOnError, false);
        }


        /// <summary>   
        /// Gets AppSetting as typed value. Will return the provided defaultValue when any of the following errors happened for the provided appSetting key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <param name="defaultValue">The value that will be returned by this method when any error happenned during the read of this appSetting</param>
        /// <returns></returns>
        public static T GetValue<T>(string appSettingsKey, T defaultValue)
        {
            return GetConfigurationValueInternal(appSettingsKey, null, defaultValue, null, true);
        }

        /// <summary>
        /// Gets AppSetting as typed value. Will return the provided defaultValue if any of the following errors happened. AppSetting key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <param name="defaultValue">The value that will be returned by this method when any error happenned during the read of this appSetting</param>
        /// <param name="actionOnError">An action that will be excecuted when any error happened and can be used e.g. for logging reasons</param>
        /// <returns></returns>
        public static T GetValue<T>(string appSettingsKey, T defaultValue, Action<AppSettingReadResult, Exception> actionOnError)
        {
            return GetConfigurationValueInternal(appSettingsKey, null, defaultValue, actionOnError, true);
        }

        /// <summary>
        /// Gets AppSetting as typed value. Will return the provided defaultValue if any of the following errors happened. AppSetting key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <param name="defaultValue">The value that will be returned by this method when any error happenned during the read of this appSetting</param>
        /// <param name="converter">A converter function that will be used to convert the appSetting value (which is of string type) to the type of T (this method's type)</param>
        /// <returns></returns>
        public static T GetValue<T>(string appSettingsKey, T defaultValue, Func<string, T> converter)
        {
            return GetConfigurationValueInternal(appSettingsKey, converter, defaultValue, null, true);
        }

        /// <summary>
        /// Gets AppSetting as typed value. Will return the provided defaultValue if any of the following errors happened. AppSetting key:
        /// 1. was not found in configuration
        /// 2. was present but its value is empty
        /// 3. some error happenned during reading from configuration
        /// 4. somer error happened during conversion to typed value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="appSettingsKey">The key of the appSetting to be read.</param>
        /// <param name="defaultValue">The value that will be returned by this method when any error happenned during the read of this appSetting</param>
        /// <param name="converter">A converter function that will be used to convert the appSetting value (which is of string type) to the type of T (this method's type)</param>
        /// <param name="actionOnError">An action that will be excecuted when any error happened and can be used e.g. for logging reasons</param>
        public static T GetValue<T>(string appSettingsKey, T defaultValue, Func<string, T> converter, Action<AppSettingReadResult, Exception> actionOnError)
        {
            return GetConfigurationValueInternal(appSettingsKey, converter, defaultValue, actionOnError, true);
        }

        private static T GetConfigurationValueInternal<T>(string appSettingsKey, Func<string, T> converter, T defaultValue, Action<AppSettingReadResult, Exception> actionOnError, bool useDefaultValue)
        {
            string stringValue;
            try
            {
                stringValue = ConfigurationManager.AppSettings[appSettingsKey];
            }
            catch (Exception exception)
            {
                return HandleError(defaultValue, actionOnError, exception, string.Format(
                    "Error happened during reading appSetting with key {0} from configuration. Look inner exception",
                    appSettingsKey), AppSettingReadResult.ErrorDuringReading, useDefaultValue);
            }

            if (stringValue == null)
            {
                return HandleError(defaultValue, actionOnError, null,
                    string.Format("appSetting with key {0} is missing from configuration.", appSettingsKey), AppSettingReadResult.NotFound, useDefaultValue);

            }
            if (stringValue == string.Empty)
            {
                return HandleError(defaultValue, actionOnError, null,
                   string.Format("appSetting with key {0} has empty value.", appSettingsKey), AppSettingReadResult.EmptyValue, useDefaultValue);
            }

            if (converter == null)
            {
                converter = strValue => (T)Convert.ChangeType(strValue, typeof(T));
            }

            try
            {
                return converter(stringValue);
            }
            catch (Exception exception)
            {
                return HandleError(defaultValue, actionOnError, exception, string.Format(
                    "Error during conversion of type {0} with value {1} to type {2} for appSetting with key {3}",
                    typeof(string).Name, stringValue, typeof(T).Name, appSettingsKey), AppSettingReadResult.ConversionFailed, useDefaultValue);
            }
        }

        private static T HandleError<T>(T defaultValue, Action<AppSettingReadResult, Exception> actionOnError, Exception exception, string message, AppSettingReadResult result, bool useDefaultValue)
        {
            if (!useDefaultValue)
            {
                throw new ConfigurationErrorsException(
                    message, exception);
            }
            if (actionOnError != null)
            {
                actionOnError(result, exception);
            }
            return defaultValue;
        }
    }
}
