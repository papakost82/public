﻿namespace SimpleAppSettings
{
    public enum AppSettingReadResult
    {
        NotFound,
        EmptyValue,
        ErrorDuringReading,
        ConversionFailed,
        Success
    }
}
